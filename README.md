## Description
An AngularJS Module implementing common use cases encountered when integrating AngularJS apps
 with the precorconnect order service.

## UseCases

#####getOrderWithSapOrderNumber
Gets an order with a particular SAP Order Number

## Installation
add as bower dependency
    
```shell
bower install https://bitbucket.org/precorconnect/order-service-angularjs-sdk.git --save
```  
include in view  
```html
<script src="bower-components/angular/angular.js"></script>
<script src="bower-components/order-service-angularjs-sdk/dist/order-service-angularjs-sdk.js"></script>
```  
configure  
see below.

## Configuration 
####Properties
| Name (* denotes required) | Description |
|------|-------------|
| baseUrl* | The base url of the order service. |

#### Example
```js
angular.module(
        "app",
        ["orderServiceModule"])
        .config(
        [
            "orderServiceConfigProvider",
            appConfig
        ]);

    function appConfig(orderServiceConfigProvider) {
        orderServiceConfigProvider
            .setBaseUrl("@@orderServiceBaseUrl");
    }
```