(function () {
    angular.module(
        "orderServiceModule",
        []);
})();
(function () {
    angular
        .module("orderServiceModule")
        .provider(
        'orderServiceConfig',
        orderServiceConfigProvider
    );

    function orderServiceConfigProvider() {

        var objectUnderConstruction = {
            setBaseUrl: setBaseUrl,
            $get: $get
        };

        return objectUnderConstruction;

        function setBaseUrl(baseUrl) {
            objectUnderConstruction.baseUrl = baseUrl;
            return objectUnderConstruction;
        }

        function $get() {
            return {
                baseUrl: objectUnderConstruction.baseUrl
            }
        }
    }
})();
(function () {
    angular
        .module('orderServiceModule')
        .factory(
        'orderServiceClient',
        [
            'orderServiceConfig',
            '$http',
            '$q',
            orderServiceClient
        ]);

    function orderServiceClient(orderServiceConfig,
                                $http,
                                $q) {

        return {
            getOrderWithSapOrderNumber: getOrderWithSapOrderNumber
        };

        /**
         * @typedef {Object} Order
         * @property {string} sapOrderNumber
         * @property {string} customerSapAccountNumber
         */

        /**
         * Gets an order with a particular SAP order number
         * @returns {Order}
         */
        function getOrderWithSapOrderNumber(sapOrderNumber) {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve({
                sapOrderNumber:sapOrderNumber,
                customerSapAccountNumber:"klskjdfl23423k32k3"
            });
            return deferred.promise;
            /*
             var request = $http({
             method: "get",
             url: orderServiceConfig.baseUrl + "/orders/" + sapOrderNumber
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }

        function handleError(response) {
            if (
                !angular.isObject(response.data) || !response.data.message
            ) {

                return ( $q.reject("An unknown error occurred.") );

            }

            // Otherwise, use expected error message.
            return ( $q.reject(response.data.message) );

        }

        function handleSuccess(response) {

            return ( response.data );

        }
    }
})();
